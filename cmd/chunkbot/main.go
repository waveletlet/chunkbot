package main

import (
	"fmt"
	"os"

	. "gitlab.com/waveletlet/chunkbot"
)

func main() {
	// ============= DEBUG ====================
	// temporary writing debug info to file
	wd, err := os.Getwd()
	if err != nil {
		fmt.Printf("[Error] %s\n", err)
	}

	f, err := os.Create(wd + "debug.out")
	if err != nil {
		fmt.Printf("[Error] %s\n", err)
	}
	defer f.Close()
	// ========= END DEBUG ====================

	// get login details:
	// read email and password from stdin
	// TODO implement optional config file for auth details
	fmt.Printf("Email: ")
	var email string
	//fmt.Scanln(&email)

	fmt.Printf("Password: ")
	var password string
	//fmt.Scanln(&password)

	// temporary hardcoded testing scheisse
	email, password = "asdffdsa@gmail.com", "asdffdsa"
	fmt.Printf("\nEmail %s  Password %s\n", email, password)

	// New browser session
	bot := New()

	//err := bot.CreateAccount(email, password)
	if err = bot.Login(email, password); err != nil {
		fmt.Printf("[Error] Failed to login: %s\n", err)
	}

	testdomain := "testdomainyy.test"
	bot.CreateDomain(testdomain)
	bot.ListDomains()

	// ============= DEBUG ====================
	f.WriteString(" ===== 1 ====== ")
	f.WriteString(bot.Browser.Title())
	f.WriteString("\n(bot.Browser.Body())")
	f.WriteString(bot.Browser.Body())
	f.WriteString("\n(bot.Browser.Url())")
	f.WriteString(bot.Browser.Url().String())
	// ========= END DEBUG ====================

	domain, err := bot.GetDomain(testdomain)
	if err != nil {
		fmt.Printf("[Error] Failed to get domain: %s\n", err)
	}

	records, err := bot.ListRecords(domain)
	if err != nil {
		fmt.Printf("[Error] Failure: %s\n", err)
	}

	// ============= DEBUG ====================
	f.WriteString(" \n\n\n===== 2 ====== ")
	f.WriteString(bot.Browser.Title())
	f.WriteString("\n(bot.Browser.Body())")
	f.WriteString(bot.Browser.Body())
	f.WriteString("\n(bot.Browser.Url())")
	f.WriteString(bot.Browser.Url().String())
	// ========= END DEBUG ====================
	if len(records) > 0 {
		record, err := bot.GetRecord(records[0].ID, domain)
		if err != nil {
			fmt.Printf("[Error] Failure: %s\n", err)
		}

		newData := make(map[string]interface{})
		newData["name"] = "new name"
		newData["ttl"] = 100000
		fmt.Println(newData)
		fmt.Println(record)

		if err = bot.UpdateRecord(newData, record, domain); err != nil {
			fmt.Printf("[Error] Failure: %s\n", err)
		}

		record, err = bot.GetRecord(records[0].ID, domain)
		fmt.Println(record)
	} else {
		fmt.Println("no records to get")
	}

	// ============= DEBUG ====================
	f.WriteString(" \n\n\n===== 3 ====== ")
	// f.WriteString(bot)
	f.WriteString(bot.Browser.Title())
	f.WriteString("\n(bot.Browser.Body())")
	f.WriteString(bot.Browser.Body())
	f.WriteString("\n(bot.Browser.Url())")
	f.WriteString(bot.Browser.Url().String())
	//f.WriteString("\n(bot.Browser.ResponseHeaders())")
	//f.WriteString(bot.Browser.ResponseHeaders().String())
	//f.WriteString("\n(bot.Browser.SiteCookies())")
	//f.WriteString(bot.Browser.SiteCookies().String())
	//f.WriteString("\n(bot.Browser.CookieJar()")
	//f.WriteString(bot.Browser.CookieJar().String())
	f.Sync()
	// ========= END DEBUG ====================

	fmt.Println(bot.Browser.Title())
	bot.Logout()
	fmt.Println(bot.Browser.Title())
}
