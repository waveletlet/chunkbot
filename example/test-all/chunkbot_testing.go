// fakereal test / example code
// really i should be using a mocked server and not be making chunkhost's logs all screwy....

package main

import (
	"fmt"

	. "github.com/logrusorgru/aurora"
	. "gitlab.com/waveletlet/chunkbot"
)

const (
	validEmail    = ""
	validPassword = ""
)

func errorExpected(err error) error {
	if err != nil {
		fmt.Println(Green("[[ Success! ]]\tError present"))
	} else {
		fmt.Println(Red("[[ Fail! ]]\tExpected error not raised"))
	}
	return nil
}

func noErrorExpected(err error) error {
	if err == nil {
		fmt.Println(Green("[[ Success! ]]\tNo error"))
	} else {
		fmt.Println(Sprintf(Red("[[ Fail! ]]\tUnexpected error present: %s"), err))
	}
	return nil
}

// TODO make a function for comparing two arbitrary object
// using an interface to pass in arbitrary params and use type switch to determine data type
//func expectEquals() error {
//}

func testAuthentification() {
	// Running all these failed login tests against the live server makes you look annoying to Chunkhost and/or Cloudflare and you will probably not be able to log in for a while
	fmt.Println(Blue("====== AUTHENTIFICATION ======"))
	var email string
	var password string
	var err error

	fmt.Println(Blue("====== Testing Invalid ======"))
	email, password = "asdffdsagmail.com", "sdffd"
	//
	//		All invalid
	_, err = CreateAccount(email, password)
	err = errorExpected(err)
	//
	_, err = Login(email, password)
	err = errorExpected(err)
	//
	//		Valid user, wrong password
	email, password = "asdffdsa@gmail.com", "sdffd"
	_, err = Login(email, password)
	err = errorExpected(err)

	fmt.Println(Blue("====== Testing Valid ======"))
	email, password = "asdffdsa@gmail.com", "asdffdsa"
	// TODO: needs to make a random user/pass to actually test, but maybe it's not necessary
	//err = bot.CreateAccount(email, password)
	_, err = Login(email, password)
	err = noErrorExpected(err)
}

func testDomains() {
	fmt.Println(Blue("====== DOMAINS ==============="))
	var email string
	var password string
	fmt.Println(Blue("====== Testing Invalid ======"))
	email, password = "asdffdsa@gmail.com", "asdffdsa"
	bot, _ := Login(email, password)
	email, password = "", ""
	// "Pending accounts can't create domains"
	if bot.IsLoggedIn() {
		_, err := bot.CreateDomain("teststring.test")
		err = errorExpected(err)

		fmt.Println(Blue("====== Testing Valid ======"))
		_, err = bot.ListDomains()
		err = noErrorExpected(err)
	}
	// Other errors to be aware of:
	// "Name has already been taken"
	// "Name must look like a domain name."

	fmt.Println(Blue("====== Testing Valid ======"))
	// NEED TO TEST WITH VALID ACCOUNT (or set up mocking)
	email, password = validEmail, validPassword
	bot, _ = Login(email, password)
	if bot.IsLoggedIn() {
		domainString := "testdomainyy.test"

		_, err := bot.ListDomains()
		err = noErrorExpected(err)
		domainNew, err := bot.CreateDomain(domainString)
		err = noErrorExpected(err)
		domain, err := bot.GetDomain(domainString)
		err = noErrorExpected(err)
		// TODO use expectEquals() to compare
		fmt.Printf("CreateDomain and GetDomain returned same domain? %v\n", (domain.Url == domainNew.Url))

		err = bot.DestroyDomain(domainString)
		err = noErrorExpected(err)
		fmt.Println(Blue("====== Testing Invalid ======"))
		_, err = bot.GetDomain(domainString)
		err = errorExpected(err)
		bot.Logout()
	} else {
		fmt.Println(Red("Not logged in, couldn't test domains"))
	}
}

func testRecords() {
	fmt.Println(Blue("====== RECORDS ==============="))
	var email string
	var password string
	// NEED TO TEST WITH VALID ACCOUNT (or set up mocking)
	email, password = validEmail, validPassword
	bot, _ := Login(email, password)
	if bot.IsLoggedIn() {
		domainString := "testdomainyy.test"
		domain, err := bot.GetDomain(domainString)
		if err != nil {
			domain, err = bot.CreateDomain(domainString)
			if err != nil {
				panic(Sprintf("Error creating domains: %s\nExiting...", err))
			}
		}

		fmt.Println(Blue("====== Testing Invalid ======"))
		err = bot.CreateRecord(map[string]interface{}{
			"name": "bad record",
			"kind": "invalid",
			"mx":   0,
			"ttl":  0,
		}, domain)
		err = errorExpected(err)

		fmt.Println(Blue("====== Testing Valid ======"))
		err = bot.CreateRecord(map[string]interface{}{
			"name":    "named-record",
			"kind":    "A",
			"mx":      1,
			"ttl":     60,
			"content": "content",
		}, domain)
		err = noErrorExpected(err)

		records, err := bot.ListRecords(domain)
		err = noErrorExpected(err)
		if len(records) > 0 {
			record, err := bot.GetRecord(records[0].ID, domain)
			err = noErrorExpected(err)

			newData := map[string]interface{}{
				"name": "newname",
				"ttl":  1000,
			}
			err = bot.UpdateRecord(newData, record, domain)
			err = noErrorExpected(err)

			fmt.Println(Blue("====== Testing Invalid ======"))
			newData["name"] = "bad name"
			newData["ttl"] = 100000
			err = bot.UpdateRecord(newData, record, domain)
			err = errorExpected(err)

			fmt.Println(Blue("====== Testing Valid ======"))
			err = bot.DestroyRecord(record, domain)
			err = noErrorExpected(err)
		} else {
			fmt.Println(Red("Error in record creation, couldn't test record Get/Update/Destroy"))
		}
		fmt.Println(Blue("====== Testing Invalid ======"))
		_, err = bot.GetRecord("nonexistent", domain)
		err = errorExpected(err)
	} else {
		fmt.Println(Red("Not logged in, couldn't test records"))
	}
}

func main() {
	//testAuthentification()
	//testDomains()
	testRecords()

}

// NOTE //
// Tests should test against the normal/verified account as well as pending account cases
// Tests with good account info should be split off from tests with bad account info

// UNCHECKED //
// New() *Bot
// (self *Bot) checkHtmlErrors() error
// (self *Bot) ListChunks() error
// (self *Bot) GetChunkIP(chunk Chunk) error
// (self *Bot) GetAccount() error
