// Add XMPP Record
// A simple interactive program for creating XMPP SRV records in the following format:
// _service._proto.name.			TTL	class	SRV priority weight port target.
//
// For example,
// _xmpp-server._tcp.example.com.	60	IN		SRV	10 10 5269 xmpp.example.com

package main

import (
	"fmt"
	"os"
	"strings"

	"gitlab.com/waveletlet/chunkbot"
)

type InputType int

const (
	domain InputType = iota
	service
	proto
	mx
	ttl
	weight
	port
	target
)

func askAndGetInput(input InputType, defaultLike ...string) string {
	var askText string
	var userInput string
	var defaultValue string

	switch input {
	case domain:
		askText = fmt.Sprintf("Enter domain to edit: ")
	case service:
		defaultValue = "xmpps-client"
		askText = fmt.Sprintf("Service type: (xmpps-client, xmpps-server, xmpp-client, xmpp-server, default %s) ", defaultValue)
	case proto:
		defaultValue = "tcp"
		askText = fmt.Sprintf("Protocol type: (tcp, udp, default %s) ", defaultValue)
	case mx:
		defaultValue = "10"
		askText = fmt.Sprintf("MX priority: (1-65535, default %s) ", defaultValue)
	case ttl:
		defaultValue = "3600"
		askText = fmt.Sprintf("TTL: (60-172800, default %s) ", defaultValue)
	case weight:
		defaultValue = "10"
		askText = fmt.Sprintf("Weight: (default %s) ", defaultValue)
	case port:
		switch strings.ToLower(defaultLike[0]) {
		case "xmpps-server", "xmpp-server":
			defaultValue = "5269"
		case "xmpps-client", "xmpp-client":
			defaultValue = "5222"
		}
		askText = fmt.Sprintf("Port: (default %s) ", defaultValue)
	case target:
		defaultValue = defaultLike[0]
		askText = fmt.Sprintf("Target: (default %s) ", defaultValue)
	}

	fmt.Printf(askText)
	fmt.Scanln(&userInput)
	if (userInput == "") && (input != domain) {
		userInput = defaultValue
	}
	fmt.Printf("Selected %s\n", userInput)
	return userInput
}

func askRetryExit(step string, err error, bot *chunkbot.Bot) {
	var userInput string

	fmt.Printf("[Error] %s failed due to: %s\n", step, err)
	fmt.Printf("'Retry' or 'Exit'? ")
	fmt.Scanln(&userInput)

	userInput = strings.ToLower(userInput)
	if len(userInput) == 0 {
		userInput = " "
	}
	switch string(userInput[0]) {
	case "r":
		return
	case "e":
		fmt.Println("Exiting.....")
		bot.Logout()
		os.Exit(0)
	default:
		askRetryExit("askRetryExit", fmt.Errorf("Didn't understand input, entered: %s", userInput), bot)
	}
	return
}

func askLogin() *chunkbot.Bot {
	var bot *chunkbot.Bot
	fmt.Println("Logging in.....")
	fmt.Printf("Email: ")
	var email string
	fmt.Scanln(&email)

	fmt.Printf("Password: ")
	var password string
	fmt.Scanln(&password)

	bot, err := chunkbot.Login(email, password)
	email, password = "", ""
	if err != nil {
		askRetryExit("Login", err, bot)
		bot = askLogin()
	}
	return bot
}

func askCreateDomain(bot *chunkbot.Bot) chunkbot.Domain {
	domainName := askAndGetInput(domain)
	domainObject, err := bot.GetDomain(domainName)
	if err != nil {
		fmt.Printf("[Error] Failed to get domain %s, trying to create it... \nError was: %s", domainName, err)
		if domainObject, err = bot.CreateDomain(domainName); err != nil {
			askRetryExit("CreateDomain", err, bot)
			createNewDomainRecord(bot)
		}
	}
	return domainObject
}

func askCreateRecord(domainObject chunkbot.Domain, bot *chunkbot.Bot) {
	serviceAnswer := askAndGetInput(service)
	protoAnswer := askAndGetInput(proto)
	name := fmt.Sprintf("_%s._%s", serviceAnswer, protoAnswer)

	weightAnswer := askAndGetInput(weight)
	portAnswer := askAndGetInput(port, serviceAnswer)
	targetAnswer := askAndGetInput(target, domainObject.Name)
	content := fmt.Sprintf("%s %s %s", weightAnswer, portAnswer, targetAnswer)

	recordData := map[string]interface{}{
		"name":    name,
		"kind":    "SRV",
		"mx":      askAndGetInput(mx),
		"ttl":     askAndGetInput(ttl),
		"content": content,
	}

	if err := bot.CreateRecord(recordData, domainObject); err != nil {
		askRetryExit("CreateRecord", err, bot)
		askCreateRecord(domainObject, bot)
	} else {
		fmt.Println("Record successfully created.")
		askNew(bot)
	}
}

func askNew(bot *chunkbot.Bot) {
	var userInput string
	fmt.Printf("Add another record? (y/n) ")
	fmt.Scanln(&userInput)
	userInput = strings.ToLower(userInput)

	if len(userInput) == 0 {
		userInput = " "
	}
	switch string(userInput[0]) {
	case "y":
		createNewDomainRecord(bot)
	case "n":
		fmt.Println("Exiting....")
		bot.Logout()
		os.Exit(0)
	default:
		askRetryExit("askNew", fmt.Errorf("Didn't understand input, entered: %s", userInput), bot)
		askNew(bot)
	}
}

func createNewDomainRecord(bot *chunkbot.Bot) {
	domainObject := askCreateDomain(bot)
	askCreateRecord(domainObject, bot)
}

func main() {
	bot := askLogin()
	createNewDomainRecord(bot)
}
