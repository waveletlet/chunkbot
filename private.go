package chunkbot

import (
	"errors"
	"fmt"
	"net/url"
	"strconv"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"github.com/headzoo/surf"
	"github.com/headzoo/surf/browser"
)

func newBrowser() *browser.Browser {
	b := surf.NewBrowser()
	b.SetUserAgent("Friendly bot! See https://gitlab.com/waveletlet/chunkbot")
	return b
}

func (self *Bot) openPage(url string) error {
	if err := self.Browser.Open(url); err != nil {
		// might be worth checking out the StatusCode
		err = fmt.Errorf("Couldn't open '%s': %s", url, err)
		return err
	}
	return nil
}

func (self *Bot) checkPageErrors() error {
	if pageError := self.Browser.Find("#errorExplanation").Text(); pageError != "" {
		return errors.New(pageError)
	} else if pageError := self.Browser.Find(".flash_error").Text(); pageError != "" {
		return errors.New(pageError)
	} else if pageErrors := self.Browser.Find(".field_with_errors label"); pageErrors.Text() != "" {
		formErrors := []string{}
		pageErrors.Each(func(i int, s *goquery.Selection) {
			formErrors = append(formErrors, s.Text())
		})

		errorSpan := self.Browser.Find("span.help-block")
		errorSpan.Each(func(i int, s *goquery.Selection) {
			formErrors[i] = formErrors[i] + " " + s.Text()
		})

		errorString := strings.Join(formErrors, "\n")

		return fmt.Errorf("%d form errors:\n%s", len(formErrors), errorString)
	}
	return nil
}

func searchError(expr string, err error) error {
	err = fmt.Errorf("Couldn't find '%s': %s", expr, err)
	return err
}

func formError(f browser.Submittable, err error) error {
	err = fmt.Errorf("Couldn't submit form: %s\nForm object: %s", err, f)
	return err
}

func loggedInError() error {
	return errors.New("Not logged in")
}

func prepareRecordForm(f browser.Submittable, attrs map[string]interface{}) browser.Submittable {
	switch attrs["name"].(type) {
	case string:
		name := strings.ToLower(attrs["name"].(string))
		f.Input("record[name]", name)
	}

	switch attrs["kind"].(type) {
	case string:
		f.SelectByOptionValue("record[kind]", attrs["kind"].(string))
	case RecordKind:
		kind := attrs["kind"].(RecordKind).String()
		f.SelectByOptionValue("record[kind]", kind)
	}

	switch attrs["mx"].(type) {
	case string:
		f.Input("record[priority]", attrs["mx"].(string))
	case int:
		mx := strconv.Itoa(attrs["mx"].(int))
		f.Input("record[priority]", mx)
	}

	switch attrs["ttl"].(type) {
	case string:
		f.Input("record[ttl]", attrs["ttl"].(string))
	case int:
		ttl := strconv.Itoa(attrs["ttl"].(int))
		f.Input("record[ttl]", ttl)
	}

	switch attrs["content"].(type) {
	case string:
		f.Input("record[content]", attrs["content"].(string))
	}
	return f
}

func (self *Bot) prepareDestroyForm() url.Values {
	authToken, _ := self.Browser.Find("meta").Last().Attr("content")

	postData := url.Values{}
	postData.Set("_method", "delete")
	postData.Add("authenticity_token", authToken)
	return postData
}
